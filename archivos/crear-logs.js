console.log("estoy en crear logs");
//declaro una variable de nombre fs para referenciar el modulo fs (file system)
let fs = require("fs");

function crearArchivo(nombre_archivo, texto) {
    //creamos el archivo de nombre traza.log
    fs.writeFileSync(nombre_archivo, texto + "\n");
}

function appendArchivo(nombre_archivo, texto) {
    fs.appendFileSync(nombre_archivo, texto  + "\n");
}

//crearArchivo("nombreArchivo" , "registro de informacion");
let nombreArchivo = "logs/registro.log";
appendArchivo(nombreArchivo, "hola1");
appendArchivo(nombreArchivo, "hola2");
appendArchivo(nombreArchivo, "hola3");
appendArchivo(nombreArchivo, "hola4");
