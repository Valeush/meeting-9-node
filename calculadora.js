function suma(n1 , n2) {
   return n1 + n2;
}

function resta(n1 , n2) {
    return n1 - n2;
}

function multiplica(n1 ,n2) {
    return n1 * n2;
    
}

function divide(n1 , n2){
   if (n2 == 0){
   return "No se puede dividir por cero";
   }
   return n1 / n2;
}

export {suma,resta,multiplica,divide};